#!/usr/bin/env bash
for i in "$@"
do
case $i in
    -h=*|--host=*)
    HOST="${i#*=}"
    shift # past argument=value
    ;;
esac
done
echo "Hostname  = ${HOST}"

# Wait until server is up
until $(curl --output /dev/null --silent --head --fail "http://${HOST}/api/events"); do
    printf '.'
    sleep 5
done

# Spam
curl "http://${HOST}/api/events" -H 'Content-Type: application/json' --data-binary '{ "title": "APAC Hackathon 2018 - Opening", "speaker": "John", "topic": "Frontend", "ongoing": false, "start": "2018-11-20T10:30", "end": "2018-11-20T12:00" }'
curl "http://${HOST}/api/events" -H 'Content-Type: application/json' --data-binary '{ "title": "Judging", "speaker": "Peter", "topic": "DevOps", "ongoing": false, "start": "2018-11-20T10:30", "end": "2018-11-21T12:00" }'
curl "http://${HOST}/api/events" -H 'Content-Type: application/json' --data-binary '{ "title": "Collective Intelligence", "speaker": "Matthew", "topic": "Backend", "ongoing": false, "start": "2018-11-20T10:30", "end": "2018-11-22T12:00" }'
curl "http://${HOST}/api/events" -H 'Content-Type: application/json' --data-binary '{ "title": "Blockchain", "speaker": "Paul", "topic": "Backend", "ongoing": false, "start": "2018-11-20T10:30", "end": "2018-11-23T12:00" }'
curl "http://${HOST}/api/events" -H 'Content-Type: application/json' --data-binary '{ "title": "Zobot", "speaker": "Max", "topic": "Toolchain", "ongoing": false, "start": "2018-11-20T10:30", "end": "2018-11-24T12:00" }'

curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "admin@fnl0.com", "password": "admin", "name": "kerrigan" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "ofer@fnl0.com", "password": "secret", "name": "ofer" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "john@fnl0.com", "password": "secret", "name": "john" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "paul@fnl0.com", "password": "secret", "name": "paul" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "mark@fnl0.com", "password": "secret", "name": "mark" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "jun@fnl0.com", "password": "secret", "name": "jun" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "klee@fnl0.com", "password": "secret", "name": "klee" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "max@fnl0.com", "password": "secret", "name": "max" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "matthew@fnl0.com", "password": "secret", "name": "matthew" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "riaz@fnl0.com", "password": "secret", "name": "riaz" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "jacob@fnl0.com", "password": "secret", "name": "jacob" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "marco@fnl0.com", "password": "secret", "name": "marco" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "david@fnl0.com", "password": "secret", "name": "david" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "mike@fnl0.com", "password": "secret", "name": "mike" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "catherine@fnl0.com", "password": "secret", "name": "catherine" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "fay@fnl0.com", "password": "secret", "name": "fay" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "peter@fnl0.com", "password": "secret", "name": "peter" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "ian@fnl0.com", "password": "secret", "name": "ian" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "yz@fnl0.com", "password": "secret", "name": "yz" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "philip@fnl0.com", "password": "secret", "name": "philip" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "min@fnl0.com", "password": "secret", "name": "min" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "jonathan@fnl0.com", "password": "secret", "name": "jonathan" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "dave@fnl0.com", "password": "secret", "name": "dave" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "zishan@fnl0.com", "password": "secret", "name": "zishan" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "zichen@fnl0.com", "password": "secret", "name": "zichen" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "yushen@fnl0.com", "password": "secret", "name": "yushen" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "rajesh@fnl0.com", "password": "secret", "name": "rajesh" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "gupta@fnl0.com", "password": "secret", "name": "gupta" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "zohar@fnl0.com", "password": "secret", "name": "zohar" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "simon@fnl0.com", "password": "secret", "name": "simon" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "daniel@fnl0.com", "password": "secret", "name": "daniel" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "abhishek@fnl0.com", "password": "secret", "name": "abhishek" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "rajeev@fnl0.com", "password": "secret", "name": "rajeev" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "vinh@fnl0.com", "password": "secret", "name": "vinh" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "arthur@fnl0.com", "password": "secret", "name": "arthur" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "anakin@fnl0.com", "password": "secret", "name": "anakin" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "tomi@fnl0.com", "password": "secret", "name": "tomi" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "tobias@fnl0.com", "password": "secret", "name": "tobias" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "leonard@fnl0.com", "password": "secret", "name": "leonard" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "steffen@fnl0.com", "password": "secret", "name": "steffen" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "larry@fnl0.com", "password": "secret", "name": "larry" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "phil@fnl0.com", "password": "secret", "name": "phil" }'
curl "http://${HOST}/api/users/" -H 'Content-Type: application/json' --data-binary '{ "email": "cooper@fnl0.com", "password": "secret", "name": "cooper" }'


curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "paul", "target": "ofer", "topic": "Design" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "john", "target": "ofer", "topic": "Design" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "mark", "target": "ofer", "topic": "Design" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "jun", "target": "ofer", "topic": "Design" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "klee", "target": "ofer", "topic": "Design" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "max", "target": "ofer", "topic": "Design" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "yz", "target": "ofer", "topic": "Design" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "jacob", "target": "ofer", "topic": "Design" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "marco", "target": "ofer", "topic": "Design" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "david", "target": "ofer", "topic": "Design" }'

curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "paul", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "john", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "mark", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "jun", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "klee", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "max", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "yz", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "jacob", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "marco", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "david", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "cooper", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "phil", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "anakin", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "fay", "target": "john", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "vinh", "target": "john", "topic": "Toolchain" }'

curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "simon", "target": "john", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "cooper", "target": "john", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "tomi", "target": "john", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "tobias", "target": "john", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "daniel", "target": "john", "topic": "DevOps" }'

curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "paul", "target": "jacob", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "david", "target": "jacob", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "max", "target": "jacob", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "yz", "target": "jacob", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "marco", "target": "jacob", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "riaz", "target": "jacob", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "jun", "target": "jacob", "topic": "DevOps" }'

curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "max", "target": "cooper", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "yz", "target": "cooper", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "marco", "target": "cooper", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "riaz", "target": "cooper", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "jun", "target": "cooper", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "rajeev", "target": "cooper", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "gupta", "target": "cooper", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "dave", "target": "cooper", "topic": "DevOps" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "daniel", "target": "cooper", "topic": "DevOps" }'

curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "mark", "target": "fay", "topic": "Frontend" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "john", "target": "fay", "topic": "Frontend" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "paul", "target": "fay", "topic": "Frontend" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "jun", "target": "fay", "topic": "Frontend" }'

curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "mark", "target": "peter", "topic": "Frontend" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "arthur", "target": "peter", "topic": "Frontend" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "leonard", "target": "peter", "topic": "Frontend" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "larry", "target": "peter", "topic": "Frontend" }'

curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "mark", "target": "simon", "topic": "Backend" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "yz", "target": "simon", "topic": "Backend" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "ian", "target": "simon", "topic": "Backend" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "paul", "target": "simon", "topic": "Backend" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "david", "target": "simon", "topic": "Backend" }'

curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "paul", "target": "zohar", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "mark", "target": "zohar", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "jun", "target": "zohar", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "klee", "target": "zohar", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "max", "target": "zohar", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "yz", "target": "zohar", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "jacob", "target": "zohar", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "marco", "target": "zohar", "topic": "Toolchain" }'
curl "http://${HOST}/api/profiles/" -H 'Content-Type: application/json' --data-binary '{ "source": "david", "target": "zohar", "topic": "Toolchain" }'
