BUILD_VERSION=$1

echo "Unpacking $BUILD_VERSION...."
pushd backend/current
pm2 stop kerrigan
pm2 delete kerrigan
popd
pushd backend
tar xzf "./$BUILD_VERSION.tar.gz"
rm "./$BUILD_VERSION.tar.gz"
ln -s "$PWD/$BUILD_VERSION" "$PWD/current.new"
mv -T "$PWD/current.new" "$PWD/current"
popd
pushd backend/current
pm2 start src --name kerrigan
