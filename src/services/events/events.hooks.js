const search = require('feathers-nedb-fuzzy-search');

module.exports = {
  before: {
    all: [],
    find: [search({
      fields: ['code'],
      deep: true
    })],
    get: [],
    create: [
      async (context) => {
        context.data.code = Math.random().toString(36).slice(2).substring(0,4).toUpperCase();
        context.data.createdAt = new Date();
      }
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
