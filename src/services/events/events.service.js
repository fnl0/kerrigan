// Initializes the `events` service on path `/events`
const createService = require('feathers-nedb');
const createModel = require('../../models/events.model');
const hooks = require('./events.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  const events = createService(options);
  events.docs = {
    //overwrite things here.
    description: 'A service to create and query events',
    //if we want to add a mongoose style $search hook to find, we can write this:
    find: {
      parameters: [
        {
          description: 'Number of results to return',
          in: 'query',
          name: '$limit',
          type: 'integer'
        },
        {
          description: 'Number of results to skip',
          in: 'query',
          name: '$skip',
          type: 'integer'
        },
        {
          description: 'Property to sort results (-1 or 1)',
          in: 'query',
          name: '$sort',
          type: 'string'
        },
        {
          description: 'Property to query results',
          in: 'query',
          name: '$search',
          type: 'string'
        }
      ]
    },

  };
  app.use('/api/events', events);

  // Get our initialized service so that we can register hooks
  const service = app.service('/api/events');

  service.hooks(hooks);
};
