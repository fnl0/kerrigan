// Initializes the `sentiments` service on path `/sentiments`
const createService = require('feathers-nedb');
const createModel = require('../../models/sentiments.model');
const hooks = require('./sentiments.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/api/sentiments', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('/api/sentiments');

  service.hooks(hooks);
};
