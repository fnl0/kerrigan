const messages = require('./messages/messages.service.js');
const users = require('./users/users.service.js');
const events = require('./events/events.service.js');
const sentiments = require('./sentiments/sentiments.service.js');
const questions = require('./questions/questions.service.js');
const profiles = require('./profiles/profiles.service.js');
const polls = require('./polls/polls.service.js');
const pollvotes = require('./pollvotes/pollvotes.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(messages);
  app.configure(users);
  app.configure(events);
  app.configure(sentiments);
  app.configure(questions);
  app.configure(profiles);
  app.configure(polls);
  app.configure(pollvotes);
};
