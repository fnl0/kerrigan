const assert = require('assert');
const app = require('../../src/app');

describe('\'events\' service', () => {

  it('registered the service', () => {
    const service = app.service('/api/events');
    expect(service).toBeTruthy();
  });

  it('creates an event, add createdAt and code', async () => {
    const event = await app.service('/api/events').create({
      title: 'hackathon',
      speaker: 'john',
      start: '2018-11-23T10:30',
      end: '2018-11-23T12:00'
    });

    // Makes sure the password got encrypted
    assert.ok(event.code.length === 4);
    assert.ok(event.createdAt);
  });
});
