const app = require('../../src/app');

describe('\'polls\' service', () => {
  it('registered the service', () => {
    const service = app.service('/api/polls');
    expect(service).toBeTruthy();
  });
});
