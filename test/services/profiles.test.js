const app = require('../../src/app');

describe('\'profiles\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/profiles');
    expect(service).toBeTruthy();
  });
});
