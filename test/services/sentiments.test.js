const app = require('../../src/app');

describe('\'sentiments\' service', () => {
  it('registered the service', () => {
    const service = app.service('/api/sentiments');
    expect(service).toBeTruthy();
  });
});
