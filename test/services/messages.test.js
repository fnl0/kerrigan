const app = require('../../src/app');

describe('\'messages\' service', () => {
  it('registered the service', () => {
    const service = app.service('/api/messages');
    expect(service).toBeTruthy();
  });
});
